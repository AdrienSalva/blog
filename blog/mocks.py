from django.http import Http404

class Post():

	POSTS = [

		{'id' : 1, 'title' : 'First Post', 'body' : 'This is the first post'},
		{'id' : 2, 'title' : 'Second Post', 'body' : 'This is the second post'},
		{'id' : 3, 'title' : 'Third Post', 'body' : 'This is the third post'},

	]

	@classmethod
	def all(cls):
		return cls.POSTS

	@classmethod
	def find(cls, id):
		try:
			return cls.POSTS[int(id) - 1]
		except:
			raise Http404("Erreur 404: la page recherché n'existe pas, post #{} not found.".format(id))